import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import { AccountService } from '../service/account.service';
import { Account } from '../model/account';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent implements OnInit {

  accountForm: FormGroup;
  account: Account

  constructor(private accountFormBuilder: FormBuilder, private accountService: AccountService) { 
    this.accountForm = this.accountFormBuilder.group({
        id: ['', Validators.required],
        accountBalace: ['', Validators.required],
        accountType: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  onSubmit=()=>{
    this.accountService.createAccount(this.account);
  }
}
