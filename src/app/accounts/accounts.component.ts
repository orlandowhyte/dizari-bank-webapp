import { Component, OnInit } from '@angular/core';
import { AccountService } from '../service/account.service';
import { Account } from '../model/account';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  accounts: Account;

  constructor(private accountService: AccountService) { }

  ngOnInit() {
    this.accountService.getAllAccounts().subscribe(data=>{
        this.accounts = data;
        console.log(this.accounts);
    });
  }
}
