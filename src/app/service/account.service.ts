import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Account } from '../model/account';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  
  baseUrl: String = "localhost:9999";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  getAllAccounts=()=>{
    return this.http.get<Account>(this.baseUrl+'/api/get-accounts');
  }

  getAccount=(id: number)=>{
    this.http.get(this.baseUrl+'/api/get-accounts/'+id);
  }

  createAccount=(account: Account)=>{
    return this.http.post<Account>(this.baseUrl+'/api/create-account', account, this.httpOptions);
  }
}
