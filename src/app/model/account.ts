export class Account {

    id: number;
    accountBalance: number;
    accountType: string;
}
